import tensorflow as tf 
import numpy as np 
import filters as fs 
import modelfunc as mf

def placeholders(n, name_X = "X", name_Y = "Y"):

    X = tf.placeholder(dtype = tf.float32, shape = [None, n, 1], name = name_X )
    Y = tf.placeholder(dtype = tf.float32, shape = [None, n, 1], name = name_Y )

    return [X, Y]

def get_random_sample(X, Y, size):

    #sample indexes
    indxr = np.random.randint(X.shape[0], size = size).tolist()

    #samples
    X_samples = X[indxr, :, :]
    Y_samples = Y[indxr, :, :]

    return [X_samples, Y_samples]

def optimize(X_train, Y_train, l_rate, epochs = 100, lamb = 0.2, batch_size = 50):

    #counter for costs over epochs
    costs = []

    #create placeholders for X and Y
    n = X_train.shape[1]
    print(n)
    X, Y = placeholders(n)

    #Initialize params
    params = fs.params(lamb = lamb)

    #Forward propagate
    A6 = mf.for_prop(X, params)

    #Calculate cost
    cost = mf.cost(A6, Y)

    #Define optimizer (Adam)
    optimizer = tf.train.AdamOptimizer(learning_rate = l_rate, epsilon = 1).minimize(cost)

    check_op = tf.add_check_numerics_ops()

    '''Initialize all global variables'''
    init = tf.global_variables_initializer()

    '''Add ops to save/restore all variables'''
    #saver = tf.train.Saver()

    with tf.Session() as sess:
        
        '''initialize variables'''
        sess.run(init)

        '''restore variables from disk'''
        #saver.restore(sess, "/saved_prop/model.ckpt")

        A6_temp = 0

        #training loop
        for e in range(epochs):

            X_samples, Y_samples = get_random_sample(X_train, Y_train, batch_size)

            A6_temp, _, t_cost, _ = sess.run([A6, optimizer, cost, check_op], {X : X_samples, Y : Y_samples})
            
            #add cost to cost counter list
            costs.append(t_cost)

            #display cost per 10% of epoch iterations
            if epochs != 0 and e % (epochs/30) == 0:
                print(t_cost)

        print(A6_temp)

        '''save variables'''
        #save_path = saver.save(sess, "/saved_prop/model.ckpt")
        #print("Model saved in path: %s" % save_path)

    return {"cost" : cost,
            "costs" : costs,
            "params" : params}

'''
def infer(X_inf):

    tf.reset_default_graph()

    #make placeholder for X
    n = X_inf.shape[1]
    X, _ = placeholders(n, "X_inf", "Y_inf")

    #Initialize params
    params = fs.params()

    #forward propagate
    A6 = mf.for_prop(X, params)

    #Add ops to save/restore all variables
    saver = tf.train.Saver()

    with tf.Session() as sess:

        #restore variables from disk
        saver.restore(sess, "/saved_prop/model.ckpt")       

        print(A6.eval(feed_dict = {X : X_inf}, session = sess))'''