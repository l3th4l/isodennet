# IsoDenNet

Voice Isolation and denoising DNN

Dataset contribution : Black, Dawn A. A., Li, Ma and Tian, Mi. "Automatic Identification of Emotional Cues in Chinese Opera Singing", in Proc.
of 13th Int. Conf. on Music Perception and Cognition and the 5th Conference for the Asian-Pacific Society for Cognitive Sciences of Music 
(ICMPC 13-APSC0M 5 2014), Seoul, South Korea, August 2014.

## Current architecture:


![alt text](https://i.gyazo.com/ae96b42d7929e9454114b1959a5514bb.png "CNN1D")
