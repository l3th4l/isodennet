import tensorflow as tf
from tensorflow.contrib import layers as l 
import numpy as np 

def params(lamb = 0.2):

    #Kernels (11)

    #Block 0:

    K1 = tf.get_variable(name = "K1", shape = [3, 1, 32], initializer = tf.initializers.random_normal(seed = 0), regularizer = l.l2_regularizer(lamb))

    #Block 1:

    K2 = tf.get_variable(name = "K2", shape = [3, 32, 16], initializer = tf.initializers.random_normal(seed = 0), regularizer = l.l2_regularizer(lamb))

    K3 = tf.get_variable(name = "K3", shape = [4, 16, 32], initializer = tf.initializers.random_normal(seed = 0), regularizer = l.l2_regularizer(lamb))

    #Block 2:
    
    K4 = tf.get_variable(name = "K4", shape = [5, 32, 1], initializer = tf.initializers.random_normal(seed = 0), regularizer = l.l2_regularizer(lamb))

    K5 = tf.get_variable(name = "K5", shape = [7, 1, 32], initializer = tf.initializers.random_normal(seed = 0), regularizer = l.l2_regularizer(lamb))

    #Block 3:
    
    K6 = tf.get_variable(name = "K6", shape = [3, 32, 64], initializer = tf.initializers.random_normal(seed = 0), regularizer = l.l2_regularizer(lamb))

    K7 = tf.get_variable(name = "K7", shape = [4, 64, 32], initializer = tf.initializers.random_normal(seed = 0), regularizer = l.l2_regularizer(lamb))

    #Block 4:
    
    K8 = tf.get_variable(name = "K8", shape = [5, 32, 12], initializer = tf.initializers.random_normal(seed = 0), regularizer = l.l2_regularizer(lamb))

    K9 = tf.get_variable(name = "K9", shape = [7, 12, 32], initializer = tf.initializers.random_normal(seed = 0), regularizer = l.l2_regularizer(lamb))

    #Block 5:
    
    K10 = tf.get_variable(name = "K10", shape = [3, 32, 32], initializer = tf.initializers.random_normal(seed = 0), regularizer = l.l2_regularizer(lamb))

    K11 = tf.get_variable(name = "K11", shape = [5, 32, 1], initializer = tf.initializers.random_normal(seed = 0), regularizer = l.l2_regularizer(lamb))

    return {"K1" : K1,
		    "K2" : K2,
	 	    "K3" : K3,
	 	    "K4" : K4,
	 	    "K5" : K5,
		    "K6" : K6,
            "K7" : K7, 
            "K8" : K8, 
            "K9" : K9,
            "K10" : K10, 
            "K11" : K11}
            
