from matplotlib import pyplot as plt
import numpy as np
import model as mdl
import data as dat

dat_input, dat_ground_truth, path = dat.process_dat(input_files = "/male_11/pos_1_T1.wav", ground_truth_files = "/male_11/pos_1.WAV")

mdl_dat = mdl.optimize(X_train = dat_input, Y_train = dat_ground_truth, l_rate = 0.0000003, epochs = 300, lamb = 0.00, batch_size = 100)

#plot cost over epochs
def plot_costs(cs):

    plt.plot(cs)
    plt.ylabel('cost')
    plt.xlabel('iterations')
    plt.title('Learning rate = ' + str(0.02))
    plt.show()

plot_costs(mdl_dat["costs"])

#print(dat_input[0:3,:,:].shape)

#print(mdl.infer(dat_input[0:10,:,:]))