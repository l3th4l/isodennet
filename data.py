from scipy.io import wavfile as wv
import numpy as np 
import os

def get_data(_path, input_files, ground_truth_files):
    
    _, X_raw = wv.read(_path + input_files)
    _, Y_raw = wv.read(_path + ground_truth_files)

    return [X_raw, Y_raw]

def segment(X, Y, size, segment_count):
    
    #flatten data
    X_flat, Y_flat = X.flatten(), Y.flatten()

    #set random segment indexes
    indexes = np.reshape(np.random.randint(low = 0, high = (X.shape[0] - (size - 1)), size = segment_count), newshape = [segment_count, 1])
    
    #segment range indexers
    indxr = (np.array([range(size)] * segment_count) + indexes).flatten().tolist()

    #get segments
    X_indx = X_flat[indxr]
    Y_indx = Y_flat[indxr]

    #reshape flat variables
    X_reshaped = np.reshape(X_indx,[segment_count, size])
    Y_reshaped = np.reshape(Y_indx,[segment_count, size])

    #expand dimensions
    X_dims_expd = np.expand_dims(X_reshaped, axis = 2)
    Y_dims_expd = np.expand_dims(Y_reshaped, axis = 2)

    return [X_dims_expd, Y_dims_expd]

def process_dat(input_files, ground_truth_files, data_folder = "/Dataset"):

    #get path and datasets' path
    path = os.path.abspath("")
    data_folder_path = path + data_folder

    #fetch data from path
    X_raw, Y_raw = get_data(data_folder_path, input_files, ground_truth_files)

    #segment data
    X, Y = segment(X_raw, Y_raw, size = 500, segment_count = 5000)

    return X, Y, path