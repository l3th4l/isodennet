import tensorflow as tf
from tensorflow import nn
import numpy as np 

#Cost function (RMSE)
def cost(A6, Y):
    return (tf.reduce_mean(A6 - Y))**2
    
#Forward propagation
def for_prop(X, params, is_training = True):

    #Initialize filters
    K1 = params["K1"]
    K2 = params["K2"] 
    K3 = params["K3"]
    K4 = params["K4"]
    K5 = params["K5"]
    #K6 = params["K6"]
    #K7 = params["K7"]
    #K8 = params["K8"]
    #K9 = params["K9"]
    #K10 = params["K10"]
    K11 = params["K11"]

    #Batch Norm
    X_Norm = tf.layers.batch_normalization(X, training= is_training, name = "XN")

    #Conv1d
    Z1 = nn.conv1d(X_Norm, name = "Z1", filters = K1, stride = 1, padding = "SAME") 
    #ReLU
    A1 = nn.relu(Z1)
    #Batch Norm
    A1_Norm = tf.layers.batch_normalization(A1, training= is_training, name = "A1N")

    #A1 -> Z2 & A1 -> S3

    #Block 1:
    #Conv1d
    Z2 = nn.conv1d(A1_Norm, name = "Z2", filters = K2, stride = 1, padding = "SAME") 
    #ReLU
    A2 = nn.relu(Z2)

    #Conv1d
    Z3 = nn.conv1d(A2, name = "Z3", filters = K3, stride = 1, padding = "SAME") 
    #Summation (A3 + A1) : S3 -> Z4 & S3 -> S5
    S3 = Z3 + A1_Norm
    #ReLU
    #A3 = nn.relu(S3)
    #Batch Norm
    A3_Norm =  tf.layers.batch_normalization(S3, training = is_training, name = "A3N")

    #Block 2:
    #Conv1d
    Z4 = nn.conv1d(A3_Norm, name = "Z4", filters = K4, stride = 1, padding = "SAME") 
    #ReLU
    A4 = nn.relu(Z4)

    #Conv1d
    Z5 = nn.conv1d(A4, name = "Z5", filters = K5, stride = 1, padding = "SAME") 
    #Summation (A5 + S3) : S5 -> Z6 & S5 -> S7
    S5 = Z5 + A3_Norm
    #ReLU
    #A5 = nn.leaky_relu(Z5, alpha = 0)
    #Batch Norm
    S5_Norm = tf.layers.batch_normalization(S5, training = is_training, name = "S5N")

    #Conv1d
    Z11 = nn.conv1d(A3_Norm, name = "Z11", filters = K11, stride = 1, padding = "SAME") 
    #Leaky ReLU
    #A11 = nn.leaky_relu(Z11, alpha = 0.7)

    return Z11     